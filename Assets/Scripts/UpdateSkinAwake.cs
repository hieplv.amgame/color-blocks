﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateSkinAwake : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        UpdateSkin();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void UpdateSkin()
    {
        int idSkin = PlayerPrefs.GetInt("Skin");
        for (int i = 0; i < transform.childCount; i++)
        {
            if (i == idSkin)
            {
                transform.GetChild(i).gameObject.SetActive(true);
            }
            else
                transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
