﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using EasyJoystick;

public class OnChangePositon : MonoBehaviour
{
    #region Singleton Class: OnChangePositon
    public static OnChangePositon Instance;
    private void Awake()
    {
        if (Instance != null)
        {
            Instance = this;
        }
    }
    #endregion

    [SerializeField]
    public PolygonCollider2D hole2DCollider = default;
    [SerializeField]
    public PolygonCollider2D groundCollider = default;
    [SerializeField]
    public MeshCollider GeneratedMeshCollider = default;
    [SerializeField]
    public Collider GroundCollider = default;
    [SerializeField] private Joystick joystick;


    public bool tap = false;
    public bool canMoved = true;
    #region old variable
    private Vector2 mousePos;
    private float destinationX = 0;
    private float destinationY = 0;
    public float fingerSpeed = 8;
    public float fingerSensitivity = 8;
    private Vector3 centerPos;
    private Vector3 curentPos;
    #endregion
    public float mouseSpeed = 10;
    public float moveSpeed = 5;
    public float minMaxX = 1.5f;
    public float minMaxZ = 1.5f;
    public int currentLevel = 0;
    public int reachedLevel = 0;
    private Vector3 move, targetPos;
    private float xMove, zMove;
    public int levelOffset;
    public float initialScale = .5f;
    Mesh GeneratedMesh;
    private const string LastLevelComplete = "Reached";

    private void Start()
    {
        Time.timeScale = 1;
        Game.isMoving = false;
        Game.isGameover = false;
        Game.canControl = true;
        currentLevel = SceneManager.GetActiveScene().buildIndex;
    }
    private void Update()
    {

        NewInput();
        //TestMove();
        SaveLevel(currentLevel);
    }
    private void FixedUpdate()
    {
        if (transform.hasChanged)
        {
            transform.hasChanged = false;
            hole2DCollider.transform.position = new Vector2(transform.position.x, transform.position.z);
            hole2DCollider.transform.localScale = transform.localScale * initialScale;
            MakeHole2D();
            Make3DMeshCollider();
        }
    }
    // Ham dung Hole duoi dang 2D
    private void MakeHole2D()
    {
        Vector2[] PointPositions = hole2DCollider.GetPath(0);

        for (int i = 0; i < PointPositions.Length; i++)
        {
            PointPositions[i] = hole2DCollider.transform.TransformPoint(PointPositions[i]);
        }
        groundCollider.pathCount = 2;
        groundCollider.SetPath(1, PointPositions);
    }
    // Ham dung lai Ground duoi dang 3D    
    private void Make3DMeshCollider()
    {
        if (GeneratedMesh != null)
        {
            Destroy(GeneratedMesh);
        }
        GeneratedMesh = groundCollider.CreateMesh(true, true);
        GeneratedMeshCollider.sharedMesh = GeneratedMesh;

    }

    // Ham di chuyen cho Hole
    void NewInput()
    {
        if (Game.canControl && !Game.isGameover)
        {
            xMove = joystick.Horizontal();
            zMove = joystick.Vertical();
            if (xMove >= .5f)
            {
                xMove = .5f;
            }
            if (xMove <= -.5f)
            {
                xMove = -.5f;
            }
            if (zMove >= .5f)
            {
                zMove = .5f;
            }
            if (zMove <= -.5f)
            {
                zMove = -.5f;
            }

        }

        move = Vector3.Lerp(
            transform.position,
            transform.position + new Vector3(xMove, 0, zMove),
            moveSpeed * Time.deltaTime
            );
        targetPos = new Vector3(
            Mathf.Clamp(move.x, -minMaxX, minMaxX),
            move.y,
            Mathf.Clamp(move.z, -minMaxZ, minMaxZ)
            );
        transform.position = targetPos;
    }
    //void TestMove()
    //{
    //    if (Game.canControl && !Game.isGameover)
    //    {
    //        xMove = joystick.Horizontal();
    //        zMove = joystick.Vertical();
    //    }
    //    Vector3 _direction = new Vector3(xMove, 0, zMove).normalized;
    //    if (_direction.magnitude >= .1f)
    //    {
    //        characterController.Move(_direction * moveSpeed * Time.deltaTime);
    //    }
    //}
    public void SaveLevel(int i)
    {
        if (Level.Instance.objectsInScene == 0 && LastLevel() < i)
        {
            PlayerPrefs.SetInt("Reached", i);
            Debug.Log(LastLevel());
        }
    }
    public int LastLevel()
    {
        return PlayerPrefs.GetInt("Reached");
    }
}
