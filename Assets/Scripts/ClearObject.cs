﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearObject : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        //if (!Game.isGameover)
        //{
            if (other.CompareTag("Rewards"))
            {
                Level.Instance.objectsInScene--;
                _UIManager.Instance.UpdateLevelProgress();
                Destroy(other.gameObject);
            }
            if (other.CompareTag("Obstacles"))
            {
                //Game.isGameover = true;
            }
        //}
        
    }
}
