﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class HoleCleanObjects : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Rewards"))
        {
            Level.Instance.objectsInScene--;
            _UIManager.Instance.UpdateLevelProgress();
            other.gameObject.SetActive(false);
            if(Level.Instance.objectsInScene == 0)
            {
                Game.canControl = false;
                _UIManager.Instance.ShowLevelCompleteText();
                Level.Instance.PlayWinFx();

                Invoke("NextLevel", 2f);
            }
        }
        else
            if (other.CompareTag("Obstacles"))
        {
            Game.isGameover = true;
            Camera.main.transform.DOShakePosition(1f, .4f, 20, 90f)
                .OnComplete(() =>
            {
                Level.Instance.ResetCurrentLevel();
            });  
        }
    }
    void NextLevel() 
    {

        Level.Instance.LoadNextLevel();
    }
}
