﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectLevel : MonoBehaviour
{
    public List<Button> level = new List<Button>();
    // Start is called before the first frame update
    void Start()
    {
        LoadUIButton();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadLevel(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void LoadUIButton()
    {
        for(int i = 0; i < level.Count; i++)
        {
            if(i<=PlayerPrefs.GetInt("Reached")-1)
            {
                level[i].interactable = true;

            }
            else
                if(i> PlayerPrefs.GetInt("Reached"))
            {
                level[i].transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
                
                level[i].interactable = false;
            }
        }
    }
}
