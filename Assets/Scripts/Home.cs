﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Home : MonoBehaviour
{
    [SerializeField] Button playButton;
    [SerializeField] Button helpButton;
    [SerializeField] Button closeButton;
    [SerializeField] Button quitButton;
    [SerializeField] GameObject helpPanel;
    [SerializeField] GameObject skinPanel;
    [SerializeField] Button closeSkin;
    [SerializeField] Button skinButton;
    public List<Button> skin = new List<Button>();
    //public List<Button> skin = new List<Button>();

    // Start is called before the first frame update
    void Start()
    {
        playButton.onClick.AddListener(OnPlayButtonClicked);
        helpButton.onClick.AddListener(OnHelpButtonClicked);
        closeButton.onClick.AddListener(OnCloseButtonClicked);
        quitButton.onClick.AddListener(OnQuitButtonClicked);
        closeSkin.onClick.AddListener(OnCloseSkinClicked);
        skinButton.onClick.AddListener(OnSkinButtonClicked);
        
    }


    // Update is called once per frame
    void Update()
    {
        UpdateUIBtn();
    }
    void OnPlayButtonClicked()
    {
        SceneManager.LoadScene("SelectLevel");
    }
    void OnHelpButtonClicked()
    {
        helpPanel.SetActive(true);
    }
    void OnCloseButtonClicked()
    {
        helpPanel.SetActive(false);
    }
    void OnQuitButtonClicked()
    {
        Application.Quit();
    }
    void OnCloseSkinClicked()
    {
        skinPanel.SetActive(false);
    }
    void OnSkinButtonClicked()
    {
        skinPanel.SetActive(true);
    }
    public void OnskinClicked(int i)
    {
        switch (i)
        {
            case 0:
                PlayerPrefs.SetInt("Skin", 0);
                break;
            case 1:
                PlayerPrefs.SetInt("Skin", 1);
                break;
            case 2:
                PlayerPrefs.SetInt("Skin", 2);
                break;
            case 3:
                PlayerPrefs.SetInt("Skin", 3);
                break;
            case 4:
                PlayerPrefs.SetInt("Skin", 4);
                break;
            case 5:
                PlayerPrefs.SetInt("Skin", 5);
                break;
        }
    }
    public void UpdateUIBtn()
    {
        for(int i = 0; i < skin.Count; i++)
        {
            if(i != PlayerPrefs.GetInt("Skin"))
            {
                skin[i].transform.localScale = new Vector3(.8f, .8f, .8f);
            }
            else
                skin[i].transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
